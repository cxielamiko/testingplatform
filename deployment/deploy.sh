#!/bin/bash

set -x

cd /home/django/testingplatform
git reset --hard
git pull origin master

rm -rf /home/django/venv
virtualenv --python=python3.4 /home/django/venv
source /home/django/venv/bin/activate
pip install -r requirements.txt

python manage.py collectstatic --noinput

touch /etc/uwsgi/vassals/testingplatform_uwsgi.ini
